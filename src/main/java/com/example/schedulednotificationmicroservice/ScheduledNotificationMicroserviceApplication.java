package com.example.schedulednotificationmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScheduledNotificationMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScheduledNotificationMicroserviceApplication.class, args);
	}

}
